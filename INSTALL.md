
# Pre-implementation Procedure
* Install the Splunk Universal Forwader on a dedicated linux VM
* Configure 2 or more pipelines up to #cores-1

# Data Acquisition Procedure 
* Configure 2 or more pipelines up to #cores-1
	* Configure the universal forwarder to listen as follows
``` text
[tcp://9514]
source = tcp.bluecoat
sourcetype = bluecoat:proxysg:access:kv 
index = netproxy
```
* Deploy Splunk_TA_bluecoat_proxysg to the following server roles
	* UF used for collection
	* Splunk Indexers
	* Splunk Search Heads 
		* Enterprise Security
		* ITSI (used for web proxy service monitoring)
		* Ad-Hoc IT Ops/Security
* Configure each bluecoat appliance to send "main" using a customer log format as follows

	`SPLV4 $(gmttime) $(time) c-ip=$(c-ip) Content-Type=$(quot)$(rs(Content-Type))$(quot) cs-auth-group=$(cs-auth-group) cs-bytes=$(cs-bytes) cs-categories=$(cs-categories) cs-host=$(cs-host) cs-ip=$(cs-ip) cs-method=$(cs-method) cs-uri-extension=$(cs-uri-extension) cs-uri-path=$(cs-uri-path) cs-uri-port=$(cs-uri-port) cs-uri-query=$(cs-uri-query) cs-uri-scheme=$(cs-uri-scheme) cs-User-Agent=$(quot)$(cs(User-Agent))$(quot) cs-username=$(cs-username) dnslookup-time=$(dnslookup-time) duration=$(duration) rs-status=$(rs-status) rs-version=$(rs-version) s-action=$(s-action) s-ip=$(s-ip) s-sitename=$(s-sitename) s-supplier-ip=$(s-supplier-ip) s-supplier-name=$(s-supplier-name) sc-bytes=$(sc-bytes) sc-filter-result=$(sc-filter-result) sc-filter-result=$(sc-filter-result) sc-status=$(sc-status) time-taken=$(time-taken) x-bluecoat-appliance-name=$(x-bluecoat-appliance-name) x-bluecoat-appliance-primary-address=$(x-bluecoat-appliance-primary-address) x-bluecoat-application-name=$(x-bluecoat-application-name) x-bluecoat-application-operation=$(x-bluecoat-application-operation) x-bluecoat-proxy-primary-address=$(x-bluecoat-proxy-primary-address) x-bluecoat-transaction-uuid=$(x-bluecoat-transaction-uuid) x-exception-id=$(x-exception-id) x-virus-id=$(x-virus-id) c-url=$(quot)$(url)$(quot) cs-Referer=$(quot)$(cs(Referer))$(quot)` 
	